import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './block/content/content.component';

const routes: Routes = [
  {
    path: '', component: ContentComponent, children: [
      { path: '', loadChildren: () => import('./feature/home/home.module').then(m => m.HomeModule) },
      { path: 'marketplace', loadChildren: () => import('./feature/marketplace/marketplace.module').then(m => m.MarketplaceModule) },
      { path: 'users', loadChildren: () => import('./feature/users/users.module').then(m => m.UsersModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
