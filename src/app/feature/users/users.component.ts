import { Component, OnInit } from '@angular/core';
import users from '../../../assets/data/users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: any[] = [];
  selectedUser: any = {};
  
  constructor() { }

  ngOnInit(): void {
    this.users = users.sort(() => Math.random() - 0.5);
  }
  
  setSelectedUser(user: any) {
    this.selectedUser = user;
  }
  
  refreshList() {
    this.selectedUser = {};
    this.users = users.sort(() => Math.random() - 0.5);
  }

}
