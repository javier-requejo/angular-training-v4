import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent implements OnInit {

  @Input() users: any[] = [];
  @Output() selectUser = new EventEmitter();
  @Output() refreshList = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
  }

}
