import { Component, OnInit } from '@angular/core';
import items from '../../../assets/data/marketplace';
import { MarketplaceService } from '../../core/marketplace.service';

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.scss']
})
export class MarketplaceComponent implements OnInit {

  items: any[] = [];

  constructor(private marketplaceService: MarketplaceService) { }

  ngOnInit(): void {
    this.items = items;
  }
  
  selectItem(item: any) {
    this.marketplaceService.addProduct(item);
  }

}
