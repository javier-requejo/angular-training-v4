import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketplaceComponent } from './marketplace.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ItemsMarketplaceComponent } from './items-marketplace/items-marketplace.component';
import { CoreModule } from '../../core/core.module';

const routes: Routes = [
  { path: '', component: MarketplaceComponent }
];

@NgModule({
  declarations: [
    MarketplaceComponent,
    ItemsMarketplaceComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule
  ]
})
export class MarketplaceModule { }
