import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-items-marketplace',
  templateUrl: './items-marketplace.component.html',
  styleUrls: ['./items-marketplace.component.scss']
})
export class ItemsMarketplaceComponent implements OnInit {
  
  @Input() items: any[] = [];
  @Output() selectItem = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}
