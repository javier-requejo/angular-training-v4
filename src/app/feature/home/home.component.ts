import { Component, OnInit } from '@angular/core';
import users from '../../../assets/data/users';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  monthEmployee: any = {};
  
  constructor() { }

  ngOnInit(): void {
    this.monthEmployee = users[Math.floor(Math.random() * users.length)];
  }

}
