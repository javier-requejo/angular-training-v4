import { Component } from '@angular/core';
import { DashboardWidgetComponent } from '../dashboard-widget.component';
import { DashboardWidget } from '../dashboard-widget.interface';
import { DASHBOARD_WIDGET } from '../dashboard-widget.token';
import { WeatherWidgetService } from './weather-widget.service';

@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss'],
  providers: [{
    provide: DASHBOARD_WIDGET,
    useExisting: WeatherWidgetComponent
  }]
})
export class WeatherWidgetComponent extends DashboardWidgetComponent implements DashboardWidget {

  private something = [];
  
  constructor(private weatherService: WeatherWidgetService) {
    super();
  }

  ngOnInit(): void {
    this.something = this.weatherService.getSomething();
  }
  
  load(): void {
    console.log('weather load');
  }

  refresh(): void {
    console.log('weather refresh');
  }

}
