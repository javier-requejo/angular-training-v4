import { Component } from '@angular/core';
import { DashboardWidgetComponent } from '../dashboard-widget.component';
import { DashboardWidget } from '../dashboard-widget.interface';
import { DASHBOARD_WIDGET } from '../dashboard-widget.token';
import { WindWidgetService } from './wind-widget.service';

@Component({
  selector: 'app-wind-widget',
  templateUrl: './wind-widget.component.html',
  styleUrls: ['./wind-widget.component.scss'],
  providers: [{
    provide: DASHBOARD_WIDGET,
    useExisting: WindWidgetComponent
  }]
})
export class WindWidgetComponent extends DashboardWidgetComponent implements DashboardWidget {

  private something = [];
  
  constructor(private windService: WindWidgetService) {
    super();
  }

  ngOnInit(): void {
    this.something = this.windService.getSomething();
  }
  
  load(): void {
    console.log('wind load');
  }

  refresh(): void {
    console.log('wind refresh');
  }

}
