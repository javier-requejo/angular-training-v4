export interface DashboardWidget {
  load: () => void;
  refresh: () => void;
}