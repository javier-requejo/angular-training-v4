import { Component } from '@angular/core';
import { DashboardWidgetComponent } from '../dashboard-widget.component';
import { DashboardWidget } from '../dashboard-widget.interface';
import { DASHBOARD_WIDGET } from '../dashboard-widget.token';
import { SprintWidgetService } from './sprint-widget.service';

@Component({
  selector: 'app-sprint-widget',
  templateUrl: './sprint-widget.component.html',
  styleUrls: ['./sprint-widget.component.scss'],
  providers: [{
    provide: DASHBOARD_WIDGET,
    useExisting: SprintWidgetComponent
  }]
})
export class SprintWidgetComponent extends DashboardWidgetComponent implements DashboardWidget {

  private something = [];
  
  constructor(private sprintService: SprintWidgetService) {
    super();
  }

  ngOnInit(): void {
    this.something = this.sprintService.getSomething();
  }
  
  load(): void {
    console.log('sprint load');
  }
  
  refresh(): void {
    console.log('sprint refresh');
  }

}
