import { InjectionToken } from "@angular/core";
import { DashboardWidget } from "./dashboard-widget.interface";

export const DASHBOARD_WIDGET = new InjectionToken<DashboardWidget>('Dashboard Widget');