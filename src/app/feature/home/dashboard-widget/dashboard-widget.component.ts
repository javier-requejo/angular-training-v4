import { Component, ContentChild, Input, OnInit } from '@angular/core';
import { DashboardWidget } from './dashboard-widget.interface';
import { DASHBOARD_WIDGET } from './dashboard-widget.token';

@Component({
  selector: 'app-dashboard-widget',
  templateUrl: './dashboard-widget.component.html',
  styleUrls: ['./dashboard-widget.component.scss']
})
export class DashboardWidgetComponent implements OnInit {

  @Input() title: string = '';
  @Input() color: string = '';
  @Input() noRefresh: boolean = false;
  
  @ContentChild(DASHBOARD_WIDGET, { static: true })
  widget!: DashboardWidget;
    
  constructor() { }

  ngOnInit(): void {
    this.widget.load();
  }
  
  refreshWidget(): void {
    if (!this.noRefresh) {
      this.widget.refresh();
    }
  }

}
