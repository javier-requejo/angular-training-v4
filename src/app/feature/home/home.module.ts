import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { SharedModule } from '../../shared/shared.module';
import { DashboardWidgetComponent } from './dashboard-widget/dashboard-widget.component';
import { WindWidgetComponent } from './dashboard-widget/wind-widget/wind-widget.component';
import { WeatherWidgetComponent } from './dashboard-widget/weather-widget/weather-widget.component';
import { SprintWidgetComponent } from './dashboard-widget/sprint-widget/sprint-widget.component';

const routes: Routes = [
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [
    HomeComponent,
    DashboardWidgetComponent,
    WindWidgetComponent,
    WeatherWidgetComponent,
    SprintWidgetComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class HomeModule { }
