import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarketplaceService {

  private selectedProductsSource = new BehaviorSubject<any[]>([]);
  selectedProducts$ = this.selectedProductsSource.asObservable();
  
  selectedProducts: any[] = [];
  
  constructor() { }
  
  addProduct(product: any) {
    if (!this.selectedProducts.find(p => p.id === product.id)) {
      this.selectedProducts.push(product);
      this.selectedProductsSource.next(this.selectedProducts);
    }
  }
}
