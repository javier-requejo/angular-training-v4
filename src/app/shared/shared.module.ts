import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { UsersCardComponent } from './users-card/users-card.component';
import { ItemsCardComponent } from './items-card/items-card.component';



@NgModule({
  declarations: [
    UsersCardComponent,
    ItemsCardComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatMenuModule,
  ],
  exports: [
    MatButtonModule,
    UsersCardComponent,
    ItemsCardComponent,
    MatMenuModule
  ]
})
export class SharedModule { }
