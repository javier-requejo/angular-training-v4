import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-card',
  templateUrl: './users-card.component.html',
  styleUrls: ['./users-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersCardComponent implements OnInit {

  @Input() user: any;
  
  constructor() { }

  ngOnInit(): void {
  }

}
