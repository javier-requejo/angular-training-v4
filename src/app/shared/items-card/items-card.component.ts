import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-items-card',
  templateUrl: './items-card.component.html',
  styleUrls: ['./items-card.component.scss']
})
export class ItemsCardComponent implements OnInit {

  @Input() item: any;
  
  constructor() { }

  ngOnInit(): void {
  }

}
