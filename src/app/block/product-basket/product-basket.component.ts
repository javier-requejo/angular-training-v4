import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MarketplaceService } from '../../core/marketplace.service';

@Component({
  selector: 'app-product-basket',
  templateUrl: './product-basket.component.html',
  styleUrls: ['./product-basket.component.scss']
})
export class ProductBasketComponent implements OnInit {
  products: any[] = [];
  productsSubscription!: Subscription;
  
  constructor(private marketService: MarketplaceService) { }

  ngOnInit(): void {
    this.productsSubscription = this.marketService.selectedProducts$.subscribe(data => {
      this.products = data;
    });
  }
  
  ngOnDestroy() {
    this.productsSubscription.unsubscribe();
  }
}
