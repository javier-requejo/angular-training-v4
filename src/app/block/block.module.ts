import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content/content.component';
import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { FooterComponent } from './footer/footer.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ProductBasketComponent } from './product-basket/product-basket.component';
import { CoreModule } from '../core/core.module';



@NgModule({
  declarations: [
    ContentComponent,
    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    ProductBasketComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    CoreModule
  ]
})
export class BlockModule { }
